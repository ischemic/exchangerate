require 'exchangerate/version'
require 'time'
require 'nokogiri'

module Exchangerate

	def self.get_rates()
		#make xml object from source
		xml = Nokogiri::XML(open(Gem.datadir('exchangerate') + '/source.xml'))
		data = {}
		#loop through top level cube elements (the individual days)
		xml.search('Cube[@time]').each do |time|
			day = {}
			#loop through currency elements in each day
			time.search('Cube[@currency]').each do |currency|
				#create hash of currency => rate
				day[currency['currency']] = currency['rate']
			end
			#assign currency hash to day
			data[time['time']] = day
		end
		return data
	end

	def self.at(date, from, to)
		#validate date
		date = Time.parse(date).strftime('%Y-%m-%d')

		#get data
		rates = get_rates()

		#check we have data for that date
		if rates.key?(date)
			day = rates[date]

			#check we're not doing a pointless check
			if (from == day)
				raise 'Can\'t convert same currencies'
			end

			#check we know about these currencies or they're requested a euro conversion
			unless (day.key?(from) || from == 'EUR') && (day.key?(to) || to == 'EUR')
				raise 'Data not available for chosen currencies'
			end

			#if base is EUR just return to rate
			if from == 'EUR'
				return (day[to].to_f).round(5)
			end

			#if counter currency is EUR then return appropriate calcuation
			if to == 'EUR'
				return (1 / day[from].to_f).round(5)
			end

			#otherwise figure out what one of each currency equals
			from_eur = (1 / day[from].to_f).round(5)
			to_eur = (1 / day[to].to_f).round(5)

			#calcuate FX rate
			return (from_eur / to_eur).round(5)
		else
			raise 'Data not available for that date'
		end

	end

	def self.currencies()
		#get data
		rates = get_rates()

		#get a single day
		day = rates.values[0]

		return day.keys

	end

	def self.dates()
		#get data
		rates = get_rates()

		return {'first' => rates.keys.first, 'last' => rates.keys.last}

	end

end
